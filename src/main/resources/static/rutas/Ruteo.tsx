import { Route, Routes } from "react-router-dom";
import { Acceso } from "../componentes/Acceso";
import { Catalogo } from "../componentes/Catalogo";
import { Inicio } from "../componentes/Inicio";
import { Listado } from "../componentes/Listado";

export const Ruteo = () => {
  return (
    <Routes>
      <Route path="/" element={<Inicio />} />
      <Route path="/adminPro" element={<Listado/>} />
      <Route path="/cata" element={<Catalogo />} />
      <Route path="/entra" element={<Acceso />} />
    </Routes>
  );
};
