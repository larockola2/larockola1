import { Link } from "react-router-dom";
import logoMinTic from "./../assets/images/logoMintic.png";
export const Cabecera = () => {
  return (
    <div>
      <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
        <div className="container-fluid">
          <a className="navbar-brand" href="/#">
            <img src={logoMinTic} alt="" />
          </a>
          <button
            className="navbar-toggler"
            type="button"
            data-bs-toggle="collapse"
            data-bs-target="#navbarText"
            aria-controls="navbarText"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarText">
            <ul className="navbar-nav me-auto mb-2 mb-lg-0">
              <li className="nav-item">
                <Link to="/" className="nav-link">
                  Inicio
                </Link>
              </li>
              <li className="nav-item dropdown">
                <a
                  className="nav-link dropdown-toggle"
                  href="/#"
                  role="button"
                  data-bs-toggle="dropdown"
                  aria-expanded="false"
                >
                  Opciones
                </a>
                <ul className="dropdown-menu">
                  <li>
                  <Link to="/agendar" className="dropdown-item">
                  Agendar Cita
                </Link>
                  </li>
                  <li>
                  <Link to="/consultarme" className="dropdown-item">
                  Consultar Cita
                </Link>
                  </li>
                  <li>
                  <Link to="/adminPro" className="dropdown-item">
                  Administrar
                </Link>
                  </li>
                </ul>
              </li>

              <li className="nav-item">
              <Link to="/cata" className="nav-link">
                  Catalogo
                </Link>
              </li>
            </ul>
            <span className="navbar-text">
              <Link to="/entra" className="nav-link">
                Iniciar Sesión
              </Link>
            </span>
          </div>
        </div>
      </nav>
    </div>
  );
};
