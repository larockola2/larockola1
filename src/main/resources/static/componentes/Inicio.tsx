export const Inicio = () => {
    return (
      <div>
        <div className="d-flex justify-content-center">
          <div className="col-md-6 p-5 mt-5 text-bg-dark rounded-3">
            <h2 className="tituloProyecto">Mi Primer Front</h2>
            <p>
              Ejercicio realizado el 11 de agosto{" "}
              <i className="fa-solid fa-database tituloProyecto"></i>
              <br />
              GrupoO44 UIS MINTIC <br />
            </p>
          </div>
        </div>
      </div>
    );
  };
  