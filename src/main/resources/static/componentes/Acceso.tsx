export const Acceso = () => {
  return (
    <div className="d-flex justify-content-center">
      <form className="col-md-5 mt-3">
        <div className="mb-3">
          <label htmlFor="exampleInputEmail1" className="form-label">
            Correo Electronico
          </label>
          <input
            type="email"
            className="form-control"
            id="exampleInputEmail1"
            aria-describedby="emailHelp"
          />
           </div>
        <div className="mb-3">
          <label htmlFor="exampleInputPassword1" className="form-label">
           Contraseña
          </label>
          <input
            type="password"
            className="form-control"
            id="exampleInputPassword1"
          />
        </div>
        
        <button type="submit" className="btn btn-primary">
          Ingresar
        </button>
      </form>
    </div>
  );
};
