package com.gruporock.demo.Modelos;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Table;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "canciones")
public class canciones implements Serializable{

@Id
@GeneratedValue(strategy = GenerationType.IDENTITY)
@Column(name ="codCancion")
private int codCancion;

@ManyToOne 
@JoinColumn(name="codUsuario") 
private usuarios usuarios;

@ManyToOne 
@JoinColumn(name="genero") 
private genero generos;

@Column(name ="nombreCancion")
private String nombreCanc;

@Column(name="yearCancion")
private int ano;

@Column(name="nombreArts")
private String nombreArts;

@Column(name="nombreAlbum")
private String nombreAlbum;

@Column(name="yearAlbum")
private int yearAlbum;

public int getCodCancion() {
    return codCancion;
}

public void setCodCancion(int codCancion) {
    this.codCancion = codCancion;
}

public usuarios getUsuarios() {
    return usuarios;
}

public void setUsuarios(usuarios usuarios) {
    this.usuarios = usuarios;
}

public genero getGeneros() {
    return generos;
}

public void setGeneros(genero generos) {
    this.generos = generos;
}

public String getNombreCanc() {
    return nombreCanc;
}

public void setNombreCanc(String nombreCanc) {
    this.nombreCanc = nombreCanc;
}

public int getAno() {
    return ano;
}

public void setAno(int ano) {
    this.ano = ano;
}

public String getNombreArts() {
    return nombreArts;
}

public void setNombreArts(String nombreArts) {
    this.nombreArts = nombreArts;
}

public String getNombreAlbum() {
    return nombreAlbum;
}

public void setNombreAlbum(String nombreAlbum) {
    this.nombreAlbum = nombreAlbum;
}

public int getYearAlbum() {
    return yearAlbum;
}

public void setYearAlbum(int yearAlbum) {
    this.yearAlbum = yearAlbum;
}



}
