package com.gruporock.demo.Modelos;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Table;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "genero")
public class genero implements Serializable{

@Id
@GeneratedValue(strategy = GenerationType.IDENTITY)
@Column(name ="codGenero")
private int codGenero;

@Column(name="genero")
private String genero;

public int getcodGenero() {
    return codGenero;
}

public void setcodGenero(int codGenero) {
    this.codGenero = codGenero;
}

public String getgenero() {
    return genero;
}

public void setgenero(String genero) {
    this.genero = genero;
}

}

    