package com.gruporock.demo.Modelos;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Table;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name="usuarios")
public class usuarios implements Serializable {
     
@Id
@GeneratedValue(strategy = GenerationType.IDENTITY)

@Column(name="nombreusuario")
private String nombreUsuario;
     
@Column(name="apellidousuario")
private String apellidoUsuario;
     
@Column(name="codusuario")
private int codUsuario;

@Column(name="cedulaUsuario")
private int cedulaUsuario;
     
@Column(name="correo")
private String correo;
     
@Column(name="password")
private String contrasena;
     
public String getnombreUsuario () {
         return nombreUsuario;
     } 

public void setnombreUsuario (String nombreUsuario){
        this.nombreUsuario = nombreUsuario;
    }
public String getapellidoUsuario () {
         return apellidoUsuario;
     } 

public void setapellidoUsuario (String apellidoUsuario){
        this.apellidoUsuario = apellidoUsuario;
     }
    
public Integer getcodusuario () {
         return codUsuario;
     } 
     
public void setcodUsuario (int codUsuario){
         this.codUsuario = codUsuario;
     }
    
public Integer getcedulaUsuario () {
         return cedulaUsuario;
     } 
     
public void setcedulaUsuario (int cedulaUsuario){
         this.cedulaUsuario = cedulaUsuario;
     }
     
public String getcorreo () {
         return correo;
     } 
     
public void setcorreo (String correo){
         this.correo = correo;
     }
     
public String getcontrasena () {
         return contrasena;
     } 
     
public void setcontrasena (String contrasena){
         this.correo = contrasena;
     }
    
}
