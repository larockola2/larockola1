package com.gruporock.demo.Services.Implement;

import com.gruporock.demo.Dao.cancionesDao;
import com.gruporock.demo.Modelos.canciones;
import com.gruporock.demo.Services.cancionesServices;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired; 
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class cancionesImplement implements cancionesServices {

@Autowired
private cancionesDao cancionesDao;

    @Override
    @Transactional(readOnly=false)
    public canciones save(canciones canciones) {
      
        return cancionesDao.save(canciones);
    }

    @Override
    @Transactional(readOnly=false)
    public void delete(Integer id) {
       
        cancionesDao.deleteById(id);
    }

    @Override
    @Transactional(readOnly=true)
    public canciones findById(Integer id) {
        
        return cancionesDao.findById(id).orElse(null);
    }

    @Override
    @Transactional(readOnly=true)
    public List<canciones> findAll() {
        
        return (List<canciones>) cancionesDao.findAll();
    }


}
