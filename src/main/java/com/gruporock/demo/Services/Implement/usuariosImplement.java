package com.gruporock.demo.Services.Implement;

import com.gruporock.demo.Dao.usuariosDao;
import com.gruporock.demo.Modelos.usuarios;
import com.gruporock.demo.Services.usuariosServices;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired; 
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class usuariosImplement implements usuariosServices {

@Autowired
private usuariosDao usuariosDao;

    @Override
    @Transactional(readOnly=false)
    public usuarios save(usuarios usuarios) {
        
        return usuariosDao.save(usuarios);
    }



    @Override
    @Transactional(readOnly=false)
    public void delete(Integer id) {
        
        usuariosDao.deleteById(id);
        
    }



    @Override
    @Transactional(readOnly=true)
    public usuarios findById(Integer id) {
        
        return usuariosDao.findById(id).orElse(null);
    }



    @Override
    @Transactional(readOnly=true)
    public List<usuarios> findAll() {
        
        return (List<usuarios>) usuariosDao.findAll();
    }
}
