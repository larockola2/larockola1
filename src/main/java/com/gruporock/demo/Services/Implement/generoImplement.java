package com.gruporock.demo.Services.Implement;

import com.gruporock.demo.Dao.generoDao;
import com.gruporock.demo.Modelos.genero;
import com.gruporock.demo.Services.generoServices;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired; 
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class generoImplement implements generoServices {
    
@Autowired
private generoDao generoDao;

        @Override
        @Transactional(readOnly=false)
        public genero save(genero generos) {
           
            return generoDao.save(generos);
        }

        @Override
        @Transactional(readOnly=false)
        public void delete(Integer id) {
            
            generoDao.deleteById(id);
        }

        @Override
        @Transactional(readOnly=true)
        public genero findById(Integer id) {
            
            return generoDao.findById(id).orElse(null);
        }

        @Override
        @Transactional(readOnly=true)
        public List<genero> findAll() {
            
            return (List<genero>) generoDao.findAll();
        }
    
}