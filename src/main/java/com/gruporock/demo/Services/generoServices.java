package com.gruporock.demo.Services;

import java.util.List;
import com.gruporock.demo.Modelos.genero;

public interface generoServices {
    
public genero save(genero generos);
public void delete(Integer id);
public genero findById(Integer id); 
public List<genero> findAll();

}
