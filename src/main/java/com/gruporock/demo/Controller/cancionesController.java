package com.gruporock.demo.Controller;

import com.gruporock.demo.Modelos.canciones;
import com.gruporock.demo.Services.cancionesServices;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired; 
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin; 
import org.springframework.web.bind.annotation.DeleteMapping; 
import org.springframework.web.bind.annotation.GetMapping; 
import org.springframework.web.bind.annotation.PathVariable; 
import org.springframework.web.bind.annotation.PostMapping; 
import org.springframework.web.bind.annotation.PutMapping; 
import org.springframework.web.bind.annotation.RequestBody; 
import org.springframework.web.bind.annotation.RequestMapping; 
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("*") 
@RequestMapping("/canciones")
public class cancionesController {

@Autowired
private cancionesServices cancionesServices;
    
@PostMapping(value="/")
public ResponseEntity<canciones> agregar(@RequestBody canciones canciones){ 
canciones obj = cancionesServices.save(canciones);
return new ResponseEntity<>(obj, HttpStatus.OK);
}

@DeleteMapping(value="/{id}")
public ResponseEntity<canciones> eliminar(@PathVariable Integer id){
canciones obj = cancionesServices.findById(id);

if(obj!=null)
   cancionesServices.delete(id);
else
    return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR); 
return new ResponseEntity<>(obj, HttpStatus.OK);
}

@PutMapping(value="/")
public ResponseEntity<canciones> editar(@RequestBody canciones canciones){ 
canciones obj = cancionesServices.findById(canciones.getCodCancion()); 
if(obj!=null)
{
obj.setNombreCanc(canciones.getNombreCanc());
obj.setNombreArts(canciones.getNombreArts()); 
obj.setNombreAlbum(canciones.getNombreAlbum()); 
obj.setYearAlbum(canciones.getYearAlbum());
obj.setCodCancion(canciones.getCodCancion());
obj.setAno(canciones.getAno());
cancionesServices.save(obj); }
else
return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR); 
return new ResponseEntity<>(obj, HttpStatus.OK);
}

@GetMapping("/list")
public List<canciones> consultarTodo(){ 
return cancionesServices.findAll();
}

@GetMapping("/list/{id}")
public canciones consultaPorId(@PathVariable Integer id){ 
    return cancionesServices.findById(id);
}

}
