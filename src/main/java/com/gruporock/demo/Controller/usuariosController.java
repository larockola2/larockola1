package com.gruporock.demo.Controller;

import com.gruporock.demo.Modelos.usuarios;
import com.gruporock.demo.Services.usuariosServices;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired; 
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin; 
import org.springframework.web.bind.annotation.DeleteMapping; 
import org.springframework.web.bind.annotation.GetMapping; 
import org.springframework.web.bind.annotation.PathVariable; 
import org.springframework.web.bind.annotation.PostMapping; 
import org.springframework.web.bind.annotation.PutMapping; 
import org.springframework.web.bind.annotation.RequestBody; 
import org.springframework.web.bind.annotation.RequestMapping; 
import org.springframework.web.bind.annotation.RestController;


@RestController 
@CrossOrigin("*") 
@RequestMapping("/usuarios")
public class usuariosController {

@Autowired
private usuariosServices usuariosServices;
    
@PostMapping(value="/")
public ResponseEntity<usuarios> agregar(@RequestBody usuarios usuarios){ 
usuarios obj = usuariosServices.save(usuarios);
return new ResponseEntity<>(obj, HttpStatus.OK);
}

@DeleteMapping(value="/{id}")
public ResponseEntity<usuarios> eliminar(@PathVariable Integer id){   
usuarios obj = usuariosServices.findById(id);

if(obj!=null)
    usuariosServices.delete(id); 

else
    return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR); 
return new ResponseEntity<>(obj, HttpStatus.OK);
}


@PutMapping(value="/")
public ResponseEntity<usuarios> editar(@RequestBody usuarios usuarios){ 
usuarios obj = usuariosServices.findById(usuarios.getcodusuario()); 
if(obj!=null)
{
obj.setnombreUsuario(usuarios.getnombreUsuario()); 
obj.setapellidoUsuario(usuarios.getapellidoUsuario()); 
obj.setcedulaUsuario(usuarios.getcedulaUsuario()); 
obj.setcodUsuario(usuarios.getcodusuario());
obj.setcontrasena(usuarios.getcontrasena());
obj.setcorreo(usuarios.getcorreo());
usuariosServices.save(obj);
 }
else
    return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR); 
return new ResponseEntity<>(obj, HttpStatus.OK);
}

@GetMapping("/list")
public List<usuarios> consultarTodo(){ 
    return usuariosServices.findAll();
}

@GetMapping("/list/{id}")
public usuarios consultaPorId(@PathVariable Integer id){ 
    return usuariosServices.findById(id);
}

}
