package com.gruporock.demo.Controller;

import com.gruporock.demo.Modelos.genero;
import com.gruporock.demo.Services.generoServices;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired; 
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin; 
import org.springframework.web.bind.annotation.DeleteMapping; 
import org.springframework.web.bind.annotation.GetMapping; 
import org.springframework.web.bind.annotation.PathVariable; 
import org.springframework.web.bind.annotation.PostMapping; 
import org.springframework.web.bind.annotation.PutMapping; 
import org.springframework.web.bind.annotation.RequestBody; 
import org.springframework.web.bind.annotation.RequestMapping; 
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("*")
@RequestMapping("/genero")
public class generoController {

@Autowired
private generoServices generoServices;

@PostMapping(value="/")
public ResponseEntity<genero> agregar(@RequestBody genero genero){
genero obj = generoServices.save(genero);
return new ResponseEntity<>(obj, HttpStatus.OK);
}

@DeleteMapping(value="/{id}")
public ResponseEntity<genero> eliminar(@PathVariable Integer id){
genero obj = generoServices.findById(id);
if(obj!=null)
 generoServices.delete(id);
else
 return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
return new ResponseEntity<>(obj, HttpStatus.OK);
}

@PutMapping(value="/")
public ResponseEntity<genero> editar(@RequestBody genero genero){
genero obj = generoServices.findById(genero.getcodGenero());

if(obj!=null)
{
obj.setgenero(genero.getgenero());
obj.setcodGenero(genero.getcodGenero());
generoServices.save(obj);
}
else
    return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
return new ResponseEntity<>(obj, HttpStatus.OK);
}

@GetMapping("/list")
public List<genero> consultarTodo(){
return generoServices.findAll();
}


@GetMapping("/list/{id}")
public genero consultaPorId(@PathVariable Integer id){
return generoServices.findById(id);
}


    
}
