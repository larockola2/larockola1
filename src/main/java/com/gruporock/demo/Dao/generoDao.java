package com.gruporock.demo.Dao;

import com.gruporock.demo.Modelos.genero;
import org.springframework.data.repository.CrudRepository;

public interface generoDao extends CrudRepository<genero,Integer> {
    
}
